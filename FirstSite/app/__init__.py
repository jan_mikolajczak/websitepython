
try:
    from flask import Flask , url_for, request, redirect, flash
    from flask import render_template, make_response, session
    import logging
    from logging.handlers import RotatingFileHandler
    from flask_sqlalchemy import SQLAlchemy
    from sqlalchemy import text
    import os
    from itertools import cycle

except ImportError as IE:
    print(IE)




app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:eye9rpou@localhost/users'

# adding database for users and passwords

db = SQLAlchemy(app)

class Example(db.Model):
    #table name with usernames passwords
    __tablename__ = 'people'
    user_id = db.Column('user_id', db.Integer, primary_key=True)
    username = db.Column('username', db.Unicode)
    password = db.Column('password', db.Unicode)


people = Example.query.all()



@app.route('/', methods=['GET','POST'])
def index(username=None):

    if 'username' in session:
        username = session['username']
    #left some commented code to remember technlogies
    #username = request.cookies.get('username')
    return render_template("index.html",
                           title='Home',
                           username = username)

# had problem with updating css stylesheet so had to alter the name

@app.context_processor
def override_url_for():
    return dict(url_for=dated_url_for)

def dated_url_for(endpoint, **values):
    if endpoint == 'static':
        filename = values.get('filename', None)
        if filename:
            file_path = os.path.join(app.root_path,
                                     endpoint, filename)
            values['q'] = int(os.stat(file_path).st_mtime)
    return url_for(endpoint, **values)



#route for login

@app.route('/login',methods=['POST','GET'])
def login():
    error = None
    if request.method == 'POST':
        # my method for checking if password and username are
        #the same as in my database
        if valid_login(request.form.get('username'),
                       request.form.get('password'),
                       people):
            flash("Succesfully logged in")
            session['username'] = request.form.get('username')
            return redirect(url_for('index'))
            # response = make_response(redirect(url_for('index')))
            # response.set_cookie('username', request.form.get('username'))
            # return response#redirect(url_for('index', username=request.form.get('username')))
        else:
            error = "Incorrect username and password"
            app.logger.warning('Incorrect username password for user (%s)',
                               request.form.get('username'))
    return render_template('login.html', error = error)

#route for logout

@app.route('/logout')
def logout():
    session.pop('username', None)
    return redirect(url_for('index'))
    # response = make_response(redirect(url_for('index')))
    # response.set_cookie('username', '', expires=0)
    # return response

# check if password and username are the same as in database people is the table

def valid_login(username, password, people):

    result = False

    for human in people:
        if human.password==password and human.username==username:
             result = True

    return result


#route responsible for galery

@app.route('/galery', methods=['GET', 'POST'])
def galery(username=None):



    rows = 3
    columns = 3
    #make a range to pass it to template so i can iterate over it
    photorange = range(rows * columns)
    phototable = []

    #file table with photo names
    for photo in photorange:
        phototable.append("static/img/photo_%d.jpeg" %photo)

    print(phototable)

    return render_template("galery.html",
                           title='galery',
                           username=username,
                           phototable=phototable)



if __name__ == '__main__':
    app.secret_key = 'SuperSecretKey'

    #logging errors

    handler = RotatingFileHandler('error.log', maxBytes=10000, backupCount=1)
    handler.setLevel(logging.INFO)
    app.logger.addHandler(handler)

    #################
    app.run(debug=True)

